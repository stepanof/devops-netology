# devops-netology
first line

Игнорироваться будет:
1) содержимое всех папок, которые называются ".terraform", и которые находятся на любом уровне относительно корня.
2) Все файлы с расширением .tfstate
3) Все файлы, посередине которых есть выделенное точками "tfstate" (к примеру, text.tfstate.txt)
4) Все файлы crash с расширением .log
5) Все файлы с расширением .tfvars
6) Все файлы override.tf, override.tf.json
7) Все файлы, заканчивающиеся на *_override.tf, *_override.tf.json
8) Все файлы .terraformrc, terraform.rc